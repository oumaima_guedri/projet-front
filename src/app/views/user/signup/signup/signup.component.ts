import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: any = {};
  isSuccess = false;
  isFailed = false;
  errM = '';

  sendMessage: any;
  MessageService: any;
  loading = false;
  buttionText = "Submit";
  utilisateur: any;



  constructor(private registerService: LoginService , private route:Router ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {

    this.registerService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccess = true;
        this.isFailed = false;
        this.route.navigate(['/login']);
      },
      err => {
        this.errM = err.error.message;
        this.isFailed = true;
      }
    );
    this.loading = true;
                      this.buttionText = "Submiting...";
                      let user = {
                        nom: this.utilisateur.nom,
                        email: this.utilisateur.email
                      }
                      this.registerService.sendEmail(this.form).subscribe(
                        data => {
                          // let res:any = data;
                          //console.log(
                            // 👏 > 👏 > 👏 > 👏 ${utilisateur.nom} is successfully register and mail has been sent and the message id is ${res.messageId}                       );
                        },
                        err => {
                          console.log(err);
                          this.loading = false;
                          this.buttionText = "Submit";
                        },() => {
                          this.loading = false;
                          this.buttionText = "Submit";
                        }
                      );
  }



}
