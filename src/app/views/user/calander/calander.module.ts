import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalanderRoutingModule } from './calander-routing.module';
import { CalanderComponent } from './calander.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

@NgModule({
  declarations: [
    CalanderComponent
  ],
  imports: [
    CommonModule,
    CalanderRoutingModule,
    FormsModule,
    MbscModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule
  ]
})
export class CalanderModule { }
