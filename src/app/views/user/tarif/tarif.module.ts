import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TarifRoutingModule } from './tarif-routing.module';
import { TarifComponent } from './tarif.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TarifComponent
  ],
  imports: [
    CommonModule,
    TarifRoutingModule,
    FormsModule
  ]
})
export class TarifModule { }
