import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarifService } from 'src/app/service/tarif.service';

@Component({
  selector: 'app-edittarif',
  templateUrl: './edittarif.component.html',
  styleUrls: ['./edittarif.component.css']
})
export class EdittarifComponent implements OnInit {
  id:number =0;
  ta: any={'nom':'','description':'', 'prix':''};
  constructor(private servicetarif:TarifService,private router:Router,private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.ta=JSON.parse(params.special);
 }


      })
  }

  ngOnInit(): void {
  }

  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/listetarif']);

  }
  modif():void{
    this.servicetarif.update(this.ta).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/listetarif'])

     },

     error: (t:any)=> console.error(t),

     complete:()=>{}

     })

  }


}
