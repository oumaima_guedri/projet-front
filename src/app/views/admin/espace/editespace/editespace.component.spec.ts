import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditespaceComponent } from './editespace.component';

describe('EditespaceComponent', () => {
  let component: EditespaceComponent;
  let fixture: ComponentFixture<EditespaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditespaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditespaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
