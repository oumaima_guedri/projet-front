import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddespaceComponent } from './addespace.component';

describe('AddespaceComponent', () => {
  let component: AddespaceComponent;
  let fixture: ComponentFixture<AddespaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddespaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddespaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
