import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalanderRoutingModule } from './calander-routing.module';
import { CalanderComponent } from './calander.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';



@NgModule({
  declarations: [
    CalanderComponent,

  ],
  imports: [
    CommonModule,
    CalanderRoutingModule,
    MbscModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule

  ]
})
export class CalanderModule { }
