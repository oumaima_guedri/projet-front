import { Component, OnInit } from '@angular/core';
import { setOptions, MbscEventcalendarView, MbscCalendarEvent , localeFr } from '@mobiscroll/angular';
import { HttpClient } from '@angular/common/http';

setOptions({
    locale: localeFr,
    theme: 'windows',
    themeVariant: 'light',
    clickToCreate: true,
    dragToCreate: true,
    dragToMove: true,
    dragToResize: true
});

@Component({
  selector: 'app-calander',
  templateUrl: './calander.component.html',
  styleUrls: ['./calander.component.css']
})
export class CalanderComponent implements OnInit {

  constructor(private http: HttpClient) {}

  myEvents: MbscCalendarEvent[] = [];
  view = 'schedule';
  calView: MbscEventcalendarView = {
      schedule: { type: 'week' }
  };
  currentDate: any = new Date();

  ngOnInit(): void {
      this.http.jsonp < MbscCalendarEvent[] > ('https://trial.mobiscroll.com/events/?vers=5', 'callback').subscribe((resp) => {
          this.myEvents = resp;
      });
  }

  changeView(): void {
      setTimeout(() => {
          switch (this.view) {
              case 'calendar':
                  this.calView = {
                      calendar: { labels: true }
                  };
                  break;
              case 'schedule':
                  this.calView = {
                      schedule: { type: 'week' }
                  };
                  break;
          }
      });
  }

  getFirstDayOfWeek(d: Date, prev: boolean): Date {
      const day = d.getDay();
      const diff = d.getDate() - day + (prev ? -7 : 7);
      return new Date(d.setDate(diff));
  }

  navigatePage(prev: boolean): void {
      const currentDate = this.currentDate;
      if (this.view === 'calendar') {
          const prevNextPage = new Date(currentDate.getFullYear(), currentDate.getMonth() + (prev ? -1 : 1), 1);
          this.currentDate = prevNextPage;
      } else {
          const prevNextSunday = this.getFirstDayOfWeek(currentDate, prev);
          this.currentDate = prevNextSunday;
      }
  }
}
