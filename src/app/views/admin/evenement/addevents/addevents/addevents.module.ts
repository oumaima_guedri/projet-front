import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddeventsRoutingModule } from './addevents-routing.module';
import { AddeventsComponent } from './addevents/addevents.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddeventsComponent
  ],
  imports: [
    CommonModule,
    AddeventsRoutingModule,
    FormsModule,

  ]
})
export class AddeventsModule { }
