import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeuserComponent } from './listeuser.component';

const routes: Routes = [
  {path:'',component:ListeuserComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListeuserRoutingModule { }
