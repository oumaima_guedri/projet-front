import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActiviteCrudService } from 'src/app/service/activite-crud.service';

@Component({
  selector: 'app-editact',
  templateUrl: './editact.component.html',
  styleUrls: ['./editact.component.css']
})
export class EditactComponent implements OnInit {
  id:number=0;
  act:any={'nomA':'',' DescriptionA':'','imageA':''};


  constructor(private serviceact:ActiviteCrudService,private router:Router,private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.act=JSON.parse(params.special);



      }

      })
   
  }

  ngOnInit(): void {
  }

  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/listeact']);

  }
  modif():void{
    this.serviceact.update(this.act).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/listeact'])

     },

     error: (a:any)=> console.error(a),

     complete:()=>{}

     })

  }


}
