import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditactComponent } from './editact.component';

const routes: Routes = [
  {path:'',component:EditactComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditactRoutingModule { }
