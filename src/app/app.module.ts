import { MbscModule } from '@mobiscroll/angular';

import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutsModule } from './layouts/layouts.module';
import { authInterceptProviders } from './_helpers/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [ 
    MbscModule, 
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LayoutsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    
    
  
  ],
  providers: [authInterceptProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
